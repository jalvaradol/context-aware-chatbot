import logging
import telegram
from telegram.ext import (Updater,
                          CommandHandler,
                          MessageHandler,
                          Filters,
                          CallbackQueryHandler)

import commands
import constants
import context_chatbot
import messages

# diccionario con los elementos a incluir en el contexto
context_dict = {}


def main():
    bot = telegram.Bot(token=constants.BOT_TOKEN)
    print(bot.get_me())

    updater = Updater(token=constants.BOT_TOKEN)
    dispatcher = updater.dispatcher

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.INFO)

    # commands handlers
    start_handler = CommandHandler('start', commands.start)
    iteration_handler = CommandHandler('spotify', commands.init_conversation)

    # add commands handlers
    dispatcher.add_handler(start_handler)
    dispatcher.add_handler(iteration_handler)

    # add messages handlers
    dispatcher.add_handler(CallbackQueryHandler(commands.button))
    dispatcher.add_handler(MessageHandler(Filters.text, messages.echo))

    # start the Bot
    updater.start_polling()


if __name__ == '__main__':
    context_chatbot.intent_tree()
    context_chatbot.init_context(context_dict, constants.filename)
    main()
