import sqlite3
import constants
from sqlite3 import Error


# crear una conexion de base de datos a SQLite especificado por el archivo db_file
def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn


# funcion que selecciona los intents iniciales de la conversacion
def select_initial_intents():
    conn = create_connection(constants.db)
    cur = conn.cursor()
    cur.execute("SELECT * from intents WHERE initial=1")

    rows = cur.fetchall()

    for row in rows:
        print(row)

    return rows

def select_intent_by_id(id):
    conn = create_connection(constants.db)
    cur = conn.cursor()
    cur.execute("SELECT * from intents WHERE id=?", (id,))

    row = cur.fetchall()
    return row


def select_intents_by_id(list_id):
    conn = create_connection(constants.db)
    cur = conn.cursor()
    row=[]
    for id in list_id:
        cur.execute("SELECT * from intents WHERE id=?", (id,))
        row.append(cur.fetchall())
    return row


# funcion que selecciona todas las filas de la tabla
def select_all_intents():
    conn = create_connection(constants.db)
    cur = conn.cursor()
    cur.execute("SELECT * from intents")

    rows = cur.fetchall()

    return rows

