import telegram

import context_chatbot
import db_controller

# diccionario con los objetos update de la conversacion
init_update = {}

# diccionario con los intents ejecutados
executed = {}

# diccionario con los ultimos intents mostrados
last_rows = {}

# diccionario con los intents dinamicos y su prediccion
dynamic_dict = {}

def init_conversation(bot, update):
    global dynamic_dict
    # obtener intents y predicciones del modelo de prediccion
    dynamic_dict[update.message.chat_id] = context_chatbot.dynamics_intents(-1, update.message.chat_id)

    # obtener los intents dinamicos del modelo de prediccion
    dynamic_id_intents = []
    for key in dynamic_dict[update.message.chat_id]:
        dynamic_id_intents.append(key)

    # añadir los intents dinamicos a una lista auxiliar debido a que la operacion devuelve una lista de listas
    rows_aux = db_controller.select_intents_by_id(dynamic_id_intents)

    # lista con los intents iniciales
    rows = db_controller.select_initial_intents()

    # se añaden los dinamicos
    for item in rows_aux:
        rows.append(item[0])

    global init_update
    init_update[update.message.chat_id] = update

    # se añade -1 para representar los iniciales
    global executed
    executed[update.message.chat_id] = [-1]

    global last_rows
    last_rows[update.message.chat_id] = []
    last_rows[update.message.chat_id].append(rows)

    keyboard_options(bot, update, rows)


def keyboard_options(bot, update, rows):
    print(rows)
    keyboard = []

    for row in rows:
        keyboard.append([telegram.InlineKeyboardButton(text=row[2], callback_data=str(row[1]) + "+{}".format(row[0]))])

    # en el caso de no ser los intents iniciales añadir boton de back
    if rows[0][0] != 0:
        keyboard.append([telegram.InlineKeyboardButton(text="Volver atrás", callback_data='return')])

    reply_markup = telegram.InlineKeyboardMarkup(keyboard)
    update.message.reply_text('Elige entre las siguientes opciones:', reply_markup=reply_markup)


def button(bot, update):
    query = update.callback_query
    global executed
    global last_rows
    global init_update
    global dynamic_dict

    # volver a atras
    if query.data == 'return':
        # eliminar el ultimo intent ejecutado
        executed[query.message.chat_id].pop()

        # eliminar los ultimos intents anteriores
        last_rows[query.message.chat_id].pop()

        # obtener los intents anteriores
        rows = last_rows[query.message.chat_id][-1]

        bot.deleteMessage(chat_id=query.message.chat_id, message_id=query.message.message_id)

        # obtener intents y predicciones del modelo de prediccion
        dynamic_dict[query.message.chat_id] = context_chatbot.dynamics_intents(executed[query.message.chat_id][-1], query.message.chat_id)

        keyboard_options(bot, init_update[query.message.chat_id], rows)

    else:
        # obtener el id del intent ejecutado
        intent_id = query.data.split("+")[1]

        executed[query.message.chat_id].append(intent_id)

        intent = db_controller.select_intent_by_id(intent_id)

        # bot.edit_message_text(text=text = "Selected option: {}".format(intent[0][2]), chat_id=query.message.chat_id,
        # message_id=query.message.message_id)
        bot.deleteMessage(chat_id=query.message.chat_id, message_id=query.message.message_id)

        # envio de la ejecucion a la API para generar el log
        context_chatbot.intents_log(dynamic_dict[query.message.chat_id], intent_id, query.message.chat_id)

        # callback != None
        if intent[0][1] != None:
            # ejecutar la funcion correspondiente (intent[0][1] == query.data)
            globals()[intent[0][1]](bot, update)

        if intent[0][4] != None:
            # seguir con la conversacion

            # obtener los ids de los intents siguientes
            id_intents_list = intent[0][4].split(",")

            # añadir los intents siguientes a una lista auxiliar debido a que la operacion devuelve una lista de listas
            rows_aux = db_controller.select_intents_by_id(id_intents_list)

            # añadir los intents siguientes a una nueva lista
            rows = []
            for row in rows_aux:
                rows.append(row[0])


            # obtener intents y predicciones del modelo de prediccion
            dynamic_dict[query.message.chat_id] = context_chatbot.dynamics_intents(intent_id, query.message.chat_id)

            # obtener los intents dinamicos del modelo de prediccion
            dynamic_id_intents = []
            for key in dynamic_dict[query.message.chat_id]:
                dynamic_id_intents.append(key)

            # añadir los intents dinamicos a una lista auxiliar debido a que la operacion devuelve una lista de listas
            rows_dynamic = db_controller.select_intents_by_id(dynamic_id_intents)

            for item in rows_dynamic:
                rows.append(item[0])

            # añadir los nuevos intents a last_rows
            last_rows[query.message.chat_id].append(rows)

            keyboard_options(bot, init_update[query.message.chat_id], rows)
        else:
            # envio del contexto e intents a Mongo -> intentID/userID/executed_intents
            executed_list = executed[query.message.chat_id]
            context_chatbot.context_info(intent_id, query.message.chat_id, executed_list)
            executed[query.message.chat_id].clear()


def start(bot, update):
    username = str(update.message.from_user.username)
    bot.send_message(chat_id=update.message.chat_id, text="Hola " + str(
        update.message.from_user.username) + " soy Santi, un bot conversacional. Introduce los distintos comandos (/spotify) para que aparezcan las opciones de un determinado tema.")


# FUNCIONES DE LOS INTENTS
def app(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id,
                     text="Accede a la app de Spotify mediante el siguiente enlace: https://www.spotify.com/es/")


def rock(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Mostrando categorías del genero de música rock:")


def clasico(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Mostrando categorias del género de música clásica:")


def biblioteca(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Estos son los elementos actuales de tu biblioteca:")


def podcast(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Accediendo a tus podcasts...")


def radio(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Escuchando en modo radio...")


def ultimo_podcast(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Escuchando último podcast repoducido...")


def exitos_ochenta(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Escuchando éxitos de los ochenta...take on me...")


def exitos_espana(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Escuchando éxitos de España...")


def playlist_rock(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Escuchando todas las playlists guardadas de rock...")


def beach_boys(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Escuchando The Beach Boys...")


def estudiar(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Accediendo a música clásica para estudiar (ponte a ello)")


def clasicos_mix(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Reproduciendo Clásicos Mix...")


def relajante(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Canciones de música relajante mas populares...")


def beatles_best(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Aquí tienes lo mejor de los Beatles")


def submarine(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="We all live in a Yellow Submarine...")


def my_playlist(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Accediendo a tu playlist (tienes que mejorar tus gustos)")


def playlist_acdc(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Lista de canciones de ACDC...")


def mozart(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Escuchando Mozart Collection...")


def estaciones(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Las Cuatro Estaciones...")


def leyendas(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Iniciando podcast Leyendas Urbanas")


def herrera(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Señoras Señores me alegro buenos días")


def ser(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Escuchando La Ser")


def carrusel(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Esto es... Carrusel Deportivo")


def larguero(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="En la Cadena Ser El Larguero con Manu Carreño")


def selva(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Escuchando selva...")


def jardin(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Escuchando el Jardín del Edén...")


def oceano(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Escuchando océano...")


def lluvia(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Esto es una hora de lluvia")


def rio(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Escuchando ríos...")


def opera(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Iniciando álbum Noche en la Opera")


def bohemian(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Carry on, carry on...")


def live(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="There is no time for us...")


def heaven(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Iniciando álbum made in Heaven")


def let_live(bot, update):
    query = update.callback_query
    bot.send_message(chat_id=query.message.chat_id, text="Take a piece of my heart...")
