import requests, json
import constants
import db_controller
from datetime import datetime

# diccionario con los elementos a incluir en el contexto
context = {}

# diccionario con el contexto para realizar la prediccion y para su posterior envio al log
context_log = {}

# funcion encargada de mandar todos los intents para saber predicciones nodos intermedios
def intent_tree():
    intents = db_controller.select_all_intents()
    post_petition(intents, constants.tree)

# funcion encargada de cargar los elementos del fichero del contexto
def init_context(context_dict, filename):
    f = open(filename, "r")
    num_linea = 1
    for line in f:
        line = line.replace(" ", "")
        if line.find('#') == 0 or line.__eq__("\n"):
            None
        else:
            if line.find('=') == -1:
                print('[ERROR] ' + str(filename))
                print('Error in line ' + str(num_linea))
                print('Context value is not valid (' + str(line) + ')')
            else:
                key = line.split('=')[0]
                value = line.split('=')[1]
                value = value.replace("\n", "")
                context_dict[key] = value
        num_linea = num_linea + 1
    f.close()
    global context
    context = context_dict
    print(context_dict)


# funcion encargada de obtener los datos del contexto y el recorrido de los intents
def context_info(intent, userID, executed_list):
    # diccionario con los datos del contexto
    context_data = {'intent': "", 'context': {}}
    # get current date and time
    now_object = datetime.now()
    now_str = now_object.strftime('%d/%m/%Y %H:%M:%S')
    context_data['context']['timestamp'] = now_str

    # get ID of the user
    context_data['context']['userID'] = userID

    for key in context:
        if context[key] == 'true':
            if key == 'hour':
                # get hour
                context_data['context']['hour'] = now_object.hour
            elif key == 'day':
                # get day
                get_day(now_object, context_data)
            elif key == 'month':
                # get month
                get_month(now_object.month, context_data)
            elif key == 'location':
                # TODO get location
                print('location')

    # recorrido de la lista de intents ejecutados para realizar las oportunas peticiones a Mongo
    i = 0
    while i < len(executed_list):
        peticion = False
        context_data['intent'] = executed_list[i]
        context_data['executedIntent'] = executed_list[len(executed_list) - 1]

        if (i + 1) != (len(executed_list) - 1):
            peticion = True

        i += 1
        if i == len(executed_list):
            # elimino el ultimo elemento
            executed_list.pop(i - 1)
            i = 0
        else:
            if peticion:
                print(context_data)
                # peticion POST a la API del modelo de prediccion
                post_petition(context_data, constants.actions)


# funcion encargada de realizar la peticion de los intents dinamico al modelo de prediccion
def dynamics_intents(actual_intent, userID):
    global context_log
    context_log[userID] = {'intent': "", 'context': {}, 'dynamic_intent': "", 'prediction': "", 'dynamic_executed': "" }
    # get current date and time
    now_object = datetime.now()
    now_str = now_object.strftime('%d/%m/%Y %H:%M:%S')
    context_log[userID]['context']['timestamp'] = now_str

    # get ID of the user
    context_log[userID]['context']['userID'] = userID

    context_log[userID]['intent'] = actual_intent

    for key in context:
        if context[key] == 'true':
            if key == 'hour':
                # get hour
                context_log[userID]['context']['hour'] = now_object.hour
            elif key == 'day':
                # get day
                get_day(now_object,  context_log[userID])
            elif key == 'month':
                # get month
                get_month(now_object.month,  context_log[userID])
            elif key == 'location':
                # TODO get location
                print('location')
    context_json = json.dumps(context_log[userID])

    response = requests.post(constants.dynamic, context_json)
    return response.json()


def intents_log(dynamic_dict, intent_id, userID):
    global context_log
    for key, value in dynamic_dict.items():
        context_log[userID]['dynamic_intent'] = key
        context_log[userID]['prediction'] = value
        if context_log[userID]['dynamic_intent'] == intent_id:
            context_log[userID]['dynamic_executed'] = "true"
        else:
            context_log[userID]['dynamic_executed'] = "false"
        print(context_log[userID])
        post_petition(context_log[userID], constants.log)


# funcion encargada de realizar la peticion POST a la API del modelo de prediccion
def post_petition(data, url):
    context_json = json.dumps(data)

    response = requests.post(url, context_json)
    print(response)


def get_day(now_object, context_data):
    week_days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

    day = datetime.date(now_object).weekday()

    context_data['context']['day'] = week_days[day]


def get_month(month, context_data):
    switcher = {
        1: "January",
        2: "February",
        3: "March",
        4: "April",
        5: "May",
        6: "June",
        7: "July",
        8: "August",
        9: "September",
        10: "October",
        11: "November",
        12: "December"
    }
    context_data['context']['month'] = switcher.get(month, "Invalid month")
